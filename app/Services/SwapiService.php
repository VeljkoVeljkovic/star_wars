<?php

namespace App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SwapiService {
    function wookieeShuffle($array1, $array2,$childColumn, $targetColumn) {

        $count1 = count($array1); 
        $count2 = count($array2);
        $j=0;
        $i = 0;
        $l = 0;
        $arrayWithKey = [];
        while($j <= $count1-1) {   
         $countChild = count($array1[$j][$childColumn]); 
          while($i <= $countChild -1) {  
                
            while($l <= $count2 -1) {
             $k = 0;
             
             if($array1[$j][$childColumn][$i] == $array2[$l]['url']) {
                
                $change = [ "$k" => $array2[$l][$targetColumn] ]; 
                $arrayWithKey=array_merge($arrayWithKey, $change);
                $k++;
                $l++;
                
                break;
               } else {$l++;}
               
             }       
             $i++;
          } 
           $changedColumn= ["$childColumn" => $arrayWithKey ];
           $array1[$j] = array_merge($array1[$j], $changedColumn);
           $j++;
        }
       return  $array1;     
        // return response()->json(['response'=> $array1], 200);
}


public function singleShuffle($array1, $array2, $childColumn, $targetColumn) {
   $count1 = count($array1[$childColumn]); 
   $count2 = count($array2);
   $arrayChild = $array1[$childColumn];
   dd($arrayChild);
   $j=0;
   $i = 0;
   $arrayWithKey = [];
        while($j <= $count1-1) {   
          while($i <= $count2 -1) {
            $k = 0;
            
            if($arrayChild[$j] == $array2[$i]['url']) {
               
               $change = [ "$k" => $array2[$i][$targetColumn] ]; 
               $arrayWithKey=array_merge($arrayWithKey, $change);
               $k++;
               $i++;
               
               break;
              } else {$i++;}
              
            }        
            $changedColumn= ["$childColumn" => $arrayWithKey ];
           
         $j++; } 
         $array1 = array_merge($array1, $changedColumn);
         return    $array1; 

  }

function basicShuffle($array1, $array2,$childColumn, $targetColumn) {

   $count1 = count($array1); 
   $count2 = count($array2);
   $j=0;
   $i = 0;
   
   $newvalue = "";
   while($j <= $count1-1) {   
    
           
       while($i <= $count2 -1) {
        $k = 0;
        
        if($array1[$j][$childColumn] == $array2[$i]['url']) {
           
           $newvalue =  $array2[$i][$targetColumn];
           
           $k++;
           $i++;
           
           break;
          } else {$i++;}
      
     } 
      $changedColumn= ["$childColumn" => $newvalue ];
      $array1[$j] = array_merge($array1[$j], $changedColumn);
      $j++;
   }
  return  $array1;     
   // return response()->json(['response'=> $array1], 200);
}

 }