<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\URL;
use Softonic\GraphQL\Client;
use Softonic\GraphQL\ClientBuilder;
use App\Services\SwapiService;


class SwapiController extends Controller {
   function index() {
      return response()->json("", 200);
   }
  
  
 
    function films(SwapiService $swapiService) {
       $films = Http::get('https://swapi.dev/api/films/')['results'];       
       $people = Http::get('https://swapi.dev/api/people/')['results'];
       $starships = Http::get('https://swapi.dev/api/starships/')['results'];
       
      /* wookieeShuffle($parentCollection, $childCollection, "the name of the row for which we want to pull data (no id only url is specified)",
       "name of some row in target child collections");
       */
       $response = $swapiService->wookieeShuffle($films, $people, "characters", "name");
       $response1 = $swapiService->wookieeShuffle($response, $starships, "starships", "name");
      
       return response()->json(['response'=> $response1], 200);             
}

  function starships() {


    $response = Http::get('https://swapi.dev/api/people/')['results'];
    
    $countPeople = count($response);
    $j=0;
    while($j <= $countPeople-1) { 
       $countStarships = count($response[$j]['starships']);       
       $i = 0;
       $total = 0;
       while($i <= $countStarships-1) {              
          $numberOfPassengers = Http::get($response[$j]['starships'][$i])->json()['passengers'];                    
          $total = $total + $numberOfPassengers;        
          
          $i++;
       } 
        if($total<84000) {
           unset($response[$j]);
        }
        $j++;

    }
    return response()->json(['response'=> $response1], 200); 
  }
  function newPlanets() {
    $planets = Http::get('https://swapi.dev/api/planets/')['results'];    
    $newPlanets = [];
    $countPlanets = count($planets);
    $i = 0;
    while ($i <= $countPlanets-1) {
      
        if(strtotime($planets[$i]['created']) > strtotime("12-04-2014")) {
          $arrayPlanet = ["$i" => $planets[$i] ];
          $newPlanets = array_merge($newPlanets, $arrayPlanet);
        }
        $i++;
     }

     return response()->json(['response'=> $newPlanets], 200);
  }

function allActors(SwapiService $swapiService) {
   $actors = Http::get('https://swapi.dev/api/people/')['results'];
   $films = Http::get('https://swapi.dev/api/films/')['results'];
   $planets = Http::get('https://swapi.dev/api/planets/')['results'];
   $response = $swapiService->wookieeShuffle($actors, $films, "films", "title");
   $response1 = $swapiService->basicShuffle($response, $planets, "homeworld", "name" );  
    

   return response()->json(['actors'=> $response1], 200);
}
 /* function allActorsWithFilms() {

    $response = Http::get('https://swapi.dev/api/people/')['results'];
    $countPeople = count($response);
    $j=0;
    while($j <= $countPeople-1) { 
       $countFilms = count($response[$j]['films']);
       $filmsArraysWithoutKey=[];
       $i = 0;
       while($i <= $countFilms-1) {        
          $title = Http::get($response[$j]['films'][$i])->json()['title'];         
          $title = [ "$i" => "$title" ];     
          $filmsArraysWithoutKey=array_merge($filmsArraysWithoutKey, $title);
          $i++;
       } 

        $films = ["films" => $filmsArraysWithoutKey ];
        $response[$j] = array_merge($response[$j], $films);

        $j++;

    }
    dd($response);
  }  */
}
?>