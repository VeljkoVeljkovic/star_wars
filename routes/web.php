<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\URL;
use Softonic\GraphQL\Client;
use Softonic\GraphQL\ClientBuilder;
use Illuminate\Support\Collection;
/*
 Route::get('/', function(){
  $array1 = Http::get('https://swapi.dev/api/people/1')['results'];
  $array2 = Http::get('https://swapi.dev/api/films/')['results'];
  $childColumn = "films";
  $targetColumn = "title";
 
  $count1 = count($array1); 
  $count2 = count($array2);
  $j=0;
  $i = 0;
  
  $arrayWithKey = [];
  while($j <= $count1-1) {   
   
          
      while($i <= $count2 -1) {
       $k = 0;
       
       if($array1[$j][$childColumn] == $array2[$i]['url']) {
          
          $change = [ "$k" => $array2[$i][$targetColumn] ]; 
          $arrayWithKey=array_merge($arrayWithKey, $change);
          $k++;
          $i++;
          
          break;
         } else {$i++;}
     
    } 
     $changedColumn= ["$childColumn" => $arrayWithKey ];
     $array1[$j] = array_merge($array1[$j], $changedColumn);
     $j++;
  }
 return  $array1;     

  


  
});    */

  Route::get('/{any}', function(){
  return view('landing');
})->where('any', '.*');  

/*
 $id = 2;
    $client = \Softonic\GraphQL\ClientBuilder::build('https://swapi.dev/api/people/');

    $query = '{
      results {
        name
      }
    }';
    
    $variables = [
        'idFoo' => 'foo',
        'idBar' => 'bar',
    ];
    
    $response = $client->query($query, $variables); */
    
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!

*/

