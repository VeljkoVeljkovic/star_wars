<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'cors'], function() {  
    //Star Warsfront rute
     Route::get('/', 'App\Http\Controllers\SwapiController@index');
    Route::get('/films', 'App\Http\Controllers\SwapiController@films');
     Route::get('/starships', 'App\Http\Controllers\SwapiController@starships');
     Route::get('/newplanets', 'App\Http\Controllers\SwapiController@newPlanets');
     Route::get('/actors', 'App\Http\Controllers\SwapiController@allActors');  
     
    });
   