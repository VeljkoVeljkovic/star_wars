import Vue from 'vue'
 import 'es6-promise/auto';

import AOS from 'aos';
import 'aos/dist/aos.css'; 
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(VueSweetalert2); 

import JwPagination from 'jw-vue-pagination';
Vue.component('jw-pagination', JwPagination);

//import Vuelidate from 'vuelidate'
//Vue.use(Vuelidate)


import Veevalidate from 'vee-validate';
Vue.use(Veevalidate, { fieldsBagName: 'veeFields' });



//Bootrsap vue
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(VueLazyload)
import App from './App.vue';


const router = new VueRouter({
	mode: 'history',
	routes: [
 
	
		{
            path: '/',
            name: 'home',			
            component: () => import('./components/front/index.vue'),
            meta: {
                keepAlive: true
            },
            props: true
		},
	 
        {
            path: '/films',
            name: 'films',			
            component: () => import('./components/front/films.vue'),
            meta: {
                keepAlive: true
            },
            props: true
		},
        {
            path: '/starships',
            name: 'starships',			
            component: () => import('./components/front/starships.vue'),
            meta: {
                keepAlive: true
            },
            props: true
		},
        {
            path: '/newplanets',
            name: 'newplanets',			
            component: () => import('./components/front/newplanets.vue'),
            meta: {
                keepAlive: true
            },
            props: true
		},
        {
            path: '/actors',
            name: 'actors',			
            component: () => import('./components/front/actorFilms.vue'),
            meta: {
                keepAlive: true
            },
            props: true
		},
        {
            path: '/actor/films/{id}',
            name: 'actors/films',			
            component: () => import('./components/front/actorFilms.vue'),
            meta: {
                keepAlive: true
            },
            props: true
		}
        

	   
	],
})



new Vue({
	el: "#app",
	created() {
		AOS.init()
	}, 
	
	router: router,
	render: h => h(App)
})
